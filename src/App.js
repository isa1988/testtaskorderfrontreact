import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/authentication/login/Login';
import GeneralFunction from './components/GeneralFunction';
import UnitList from './components/unit/UnitList';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import UnitAdd from './components/unit/UnitAdd';
import UnitEdit from './components/unit/UnitEdit';

function App() {
  const [showFormModal, setShowFormModal] = useState(false);

  const handleShowFormModal = () => setShowFormModal(true);
  const handleCloseFormModal = () => setShowFormModal(false);

  const handleResponse = (response) => {
    GeneralFunction.setUserName(response.result.title);
  };

  useEffect(() => {
    async function loadApp() {
      console.log('11');
      const responseURL = await GeneralFunction.evenResponse(
        '/api/v1/User/userCaption',
        {},
        'get',
        handleResponse,
        true
      );
      if (responseURL === 200) {
      } else {
        handleShowFormModal();
      }
    }
    loadApp();
  }, [showFormModal, handleShowFormModal]);
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route path="unit" element={<UnitList />} />
            <Route path="unit/create" element={<UnitAdd />} />
            <Route path="unit/edit/:id" element={<UnitEdit />} />
          </Route>
        </Routes>
        <Login show={showFormModal} handleClose={handleCloseFormModal} />
      </div>
    </BrowserRouter>
  );
}

export default App;
