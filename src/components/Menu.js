import { Link } from 'react-router-dom';

const Menu = () => {
  return (
    <nav>
      <Link to="unit">Единицы измерения</Link>
    </nav>
  );
};

export default Menu;
