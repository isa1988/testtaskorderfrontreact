import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import ConfirmRole from './ConfirmRole';
import GeneralFunction from '../../GeneralFunction';

const Login = ({ show, handleClose }) => {
  const [showConfirmRoleModal, setShowConfirmRoleModal] = useState(false);
  const [formData, setFormData] = useState({
    username: '',
    password: '',
  });
  const [formDataSecond, setFormDataSecond] = useState([]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleResponse = (response) => {
    GeneralFunction.setToken(response.result.token);
    setFormDataSecond(response.result.roles);
    setShowConfirmRoleModal(true);
    handleClose();
  };

  const handleSubmit = async (e) => {
    await GeneralFunction.evenResponse(
      '/api/v1/User/login',
      {
        login: formData.username,
        password: formData.password,
      },
      'post',
      handleResponse
    );
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Авторизация</Modal.Title>
      </Modal.Header>
      <Modal.Body className='styled-form-background app-loaded naked form-view"'>
        <Form>
          <Form.Group controlId="formLogin">
            <Form.Label className="col-form-label">Логин:</Form.Label>
            <Form.Control
              type="text"
              className="form-control formInputCon"
              name="username"
              value={formData.username}
              onChange={handleChange}
            />
          </Form.Group>

          <Form.Group controlId="formPassword">
            <Form.Label className="col-form-label">Пароль:</Form.Label>
            <Form.Control
              type="password"
              className="form-control formInputCon"
              name="password"
              value={formData.password}
              onChange={handleChange}
            />
          </Form.Group>
        </Form>
        {showConfirmRoleModal && formDataSecond && (
          <ConfirmRole
            formData={formDataSecond}
            show={showConfirmRoleModal}
            handleClose={() => setShowConfirmRoleModal(false)}
          />
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleSubmit}>
          ОК
        </Button>
        <Button variant="secondary" onClick={handleClose}>
          Закрыть
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Login;
