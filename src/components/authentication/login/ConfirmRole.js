import React, { useState } from 'react';
import { Modal, Table, Form } from 'react-bootstrap';
import axios from 'axios';
import GeneralFunction from '../../GeneralFunction';

const ConfirmRole = ({ formData, show, handleClose }) => {
  const handleSubmit = async (id) => {
    try {
      const role = formData.find((x) => x.id == id);

      const response = await axios.post(
        'https://localhost:7121/api/v1/User/roleConfirm',
        {
          roleName: role.name,
          roleSys: role.sysName,
        },
        {
          headers: GeneralFunction.dataHeader(),
        }
      );

      // Handle the response as needed (e.g., store authentication token)
      console.log('Authentication successful', response.data);
      handleClose();
    } catch (error) {
      // Handle authentication failure (e.g., show an error message)
      console.error('Authentication failed', error);
    }
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Подтвежние роли</Modal.Title>
      </Modal.Header>

      <Modal.Body className='styled-form-background app-loaded naked form-view"'>
        <Form>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Системное наименование</th>
                <th>Наименование</th>
              </tr>
            </thead>
            <tbody>
              {formData.map((row) => (
                <tr key={row.id}>
                  <td onClick={() => handleSubmit(row.id)}>{row.sysName}</td>
                  <td onClick={() => handleSubmit(row.id)}>{row.name}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ConfirmRole;
