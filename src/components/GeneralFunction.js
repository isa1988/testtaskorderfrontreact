import React from 'react';
import ReactDOM from 'react-dom/client';
import axios from 'axios';
import PopupComponent from './PopupComponent';
// Setting the token in a cookie
const setTokenInCookie = (token) => {
  document.cookie = `token=${token}; expires=${new Date(
    Date.now() + 86400e3
  ).toUTCString()}; path=/`;
};

// Getting the token from cookies
const getTokenFromCookie = () => {
  const cookieArray = document.cookie.split('; ');
  for (const cookie of cookieArray) {
    const [name, value] = cookie.split('=');
    if (name === 'token') {
      return value;
    }
  }
  return null;
};

// Removing the token from cookies
const removeTokenFromCookie = () => {
  document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
};

const getTokenFromLocalStorage = () => {
  return localStorage.getItem('authToken');
};

const setTokenFromLocalStorage = (token) => {
  localStorage.setItem('authToken', token);
};

export const setToken = (token) => {
  setTokenFromLocalStorage(token);
};

export const getToken = () => {
  return getTokenFromLocalStorage();
};

export const dataHeader = () => {
  return { 'Content-Type': 'application/json', AuthorizationToken: getToken() };
};

const getUserNameFromLocalStorage = () => {
  return localStorage.getItem('userName');
};

const setUserNameFromLocalStorage = (token) => {
  localStorage.setItem('userName', token);
};

export const setUserName = (userName) => {
  setUserNameFromLocalStorage(userName);
};

export const getUserName = () => {
  return getUserNameFromLocalStorage();
};

let toastContainer = null;

export const evenResponse = async (
  url,
  requstData,
  typeRequest,
  successFun,
  isntShow
) => {
  if (!toastContainer) {
    let container = document.getElementById('popupNotification');
    toastContainer = ReactDOM.createRoot(container);
  }

  let responseCode;
  let message;
  let messageType;
  try {
    const devConfig = require('../config.dev');
    const prodConfig = require('../config.prod');

    const config =
      process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
    url = config.apiUrl + url;
    let response = {};

    if (typeRequest === 'get') {
      await axios
        .get(url, {
          headers: dataHeader(),
        })
        .then((res) => {
          response = res;
        })
        .catch((error) => {
          message = error;
          messageType = 'error';
          responseCode = 400;
        });
    } else if (typeRequest === 'post') {
      response = await axios.post(url, requstData, {
        headers: dataHeader(),
      });
    } else if (typeRequest === 'put') {
      response = await axios.put(url, requstData, {
        headers: dataHeader(),
      });
    } else if (typeRequest === 'delete') {
      await axios
        .delete(url, {
          headers: dataHeader(),
        })
        .then((res) => {
          response = res;
        })
        .catch((error) => {
          message = error;
          messageType = 'error';
          responseCode = 400;
        });
    }
    if (response) {
      if (response.data.isSuccess) {
        successFun(response.data);
        message = 'Операция прошла успеша';
        messageType = 'success';

        responseCode = 200;
      } else if (response.data.code == 6) {
        successFun(response.data);
        message = response.data.message;
        messageType = 'info';
        responseCode = 200;
      } else {
        message = response.data.message;
        messageType = 'error';
        responseCode = 400;
      }
    }
  } catch (error) {
    message = error;
    messageType = error;
    responseCode = 400;
    console.log(error);
  }

  //if (!isntShow) {
  if (toastContainer) {
    toastContainer.render(
      <React.StrictMode>
        <PopupComponent message={message} messageType={messageType} />;
      </React.StrictMode>
    );
  }
  //}
  return responseCode;
};

const GeneralFunction = {
  setToken,
  getToken,
  dataHeader,
  evenResponse,
  setUserName,
  getUserName,
};

export default GeneralFunction;
