import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import GeneralFunction from '../GeneralFunction';

const UnitEdit = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [formData, setFormData] = useState({ name: '' });

  const handleResponse = (response) => {
    setFormData(response.result);
  };

  useEffect(() => {
    const fetchUnitData = async () => {
      console.log('UnitEdit - Received id from URL:', id);
      await GeneralFunction.evenResponse(
        `/api/v1/Unit/id/${id}`,
        null,
        'get',
        handleResponse
      );
    };

    fetchUnitData();
  }, [id]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleResponseEdit = (response) => {
    if (response.isSuccess) {
      navigate('../unit');
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await GeneralFunction.evenResponse(
      '/api/v1/Unit/edit',
      {
        id: id,
        name: formData.name,
      },
      'put',
      handleResponseEdit
    );
  };

  return (
    <form className="container mt-4">
      <h1>Редактировать единицу измерения</h1>
      <div className="mb-3">
        <label htmlFor="name" className="form-label">
          Наименование:
        </label>
        <input
          type="text"
          className="form-control"
          id="name"
          name="name"
          value={formData.name}
          onChange={handleChange}
        />
      </div>
      <button type="submit" onClick={handleSubmit} className="btn btn-success">
        Сохранить
      </button>
    </form>
  );
};

export default UnitEdit;
