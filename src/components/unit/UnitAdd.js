import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import GeneralFunction from '../GeneralFunction';

const UnitAdd = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({ name: '' });

  const handleResponseAdd = (response) => {
    if (response.isSuccess) {
      navigate('../unit');
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await GeneralFunction.evenResponse(
      '/api/v1/Unit/create',
      {
        name: formData.name,
      },
      'post',
      handleResponseAdd
    );
  };

  return (
    <form className="container mt-4">
      <h1>Добавить единицу измерения</h1>
      <div className="mb-3">
        <label htmlFor="name" className="form-label">
          Наименование:
        </label>
        <input
          type="text"
          className="form-control"
          id="name"
          name="name"
          value={formData.name}
          onChange={handleChange}
        />
      </div>
      <button type="submit" onClick={handleSubmit} className="btn btn-success">
        Сохранить
      </button>
    </form>
  );
};

export default UnitAdd;
