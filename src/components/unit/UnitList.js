import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { useNavigate, useLocation } from 'react-router-dom';
import moment from 'moment';
import queryString from 'query-string';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSort,
  faSortUp,
  faSortDown,
} from '@fortawesome/free-solid-svg-icons';
import ConfirmationDialog from '../ConfirmationDialog';
import GeneralFunction from '../GeneralFunction';
import QueryParamsHelper from '../QueryParamsHelper';
import createPNG from '../../img/create.png';
import editPNG from '../../img/edit.png';
import deletePNG from '../../img/delete.png';
import reDeletePNG from '../../img/redelete.png';

const UnitList = () => {
  const [filter, setFilter] = useState({ search: '' });
  const [queryParams, setQueryParams] = useState({
    filter: JSON.stringify(filter),
    order: {
      sizeOfPage: 10,
      field: 0,
      orderDirection: 0,
      currentPage: 1,
    },
  });
  const [lastURL, setLastURL] = useState('');
  const navigate = useNavigate();
  const location = useLocation();
  const [totalItemsPerPage, setTotalItemsPerPage] = useState(0);
  const [showDelDialog, setShowDelDialog] = useState(false);

  const [formDelete, setFormDelete] = useState({});
  const [formData, setFormData] = useState([]);

  const handleAddClick = () => {
    navigate('create');
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFilter((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const stringifyJsonValues = (params) => {
    const result = {};
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        result[key] =
          typeof params[key] === 'object'
            ? JSON.stringify(params[key])
            : params[key];
      }
    }
    return result;
  };

  useEffect(() => {
    const parsedParams = QueryParamsHelper.parse(location.search);
    if (
      parsedParams &&
      lastURL !== location.search &&
      (parsedParams.filter || parsedParams.order)
    ) {
      const parsedParamsForFilrer = JSON.parse(parsedParams.filter);
      const parsedParamsForOrder = JSON.parse(parsedParams.order);

      if (
        (parsedParamsForFilrer &&
          parsedParamsForFilrer.search &&
          parsedParamsForFilrer.search !== queryParams.filter.search) ||
        (parsedParamsForOrder &&
          ((parsedParamsForOrder.sizeOfPage &&
            parsedParamsForOrder.sizeOfPage !== queryParams.order.sizeOfPage) ||
            (parsedParamsForOrder.field &&
              parsedParamsForOrder.field !== queryParams.order.field) ||
            (parsedParamsForOrder.orderDirection &&
              parsedParamsForOrder.orderDirection !==
                queryParams.order.orderDirection) ||
            (parsedParamsForOrder.currentPage &&
              parsedParamsForOrder.currentPage !==
                queryParams.order.currentPage)))
      ) {
        setLastURL(location.search);
        setQueryParams({
          filter: {
            search: parsedParamsForFilrer.search,
          },
          order: {
            sizeOfPage: parsedParamsForOrder.sizeOfPage,
            field: parsedParamsForOrder.field,
            orderDirection: parsedParamsForOrder.orderDirection,
            currentPage: parsedParamsForOrder.currentPage,
          },
        });
        if (
          parsedParamsForFilrer &&
          (!filter || filter.search !== parsedParamsForFilrer.search)
        ) {
          setFilter({
            search: parsedParamsForFilrer.search,
          });
        }
        return;
      }
    }
    const queryParamsWithJsonStringification = stringifyJsonValues(queryParams);
    const newQueryParams = queryString.stringify(
      queryParamsWithJsonStringification
    );
    window.history.pushState(null, '', `?${newQueryParams}`);

    console.log('Clicked Edit for unit with id:', newQueryParams);

    handleSubmit();
  }, [queryParams]);

  const handlePageChange = ({ selected }) => {
    if (selected === 0 || selected) {
      selected++;
      setQueryParams({
        filter: {
          search: filter.search,
        },
        order: {
          sizeOfPage: queryParams.order.sizeOfPage,
          field: queryParams.order.field,
          orderDirection: queryParams.order.orderDirection,
          currentPage: selected,
        },
      });
    }
  };

  const handleOrderFieldChangeSetQueryParams = ({
    sizeOfPage,
    field,
    orderDirection,
    currentPage,
  }) => {
    setQueryParams({
      filter: {
        search: filter.search,
      },
      order: {
        sizeOfPage: sizeOfPage,
        field: field,
        orderDirection: orderDirection,
        currentPage: currentPage,
      },
    });
  };

  const handleOrderFieldChange = ({ selected }) => {
    if (!selected) {
      return;
    } else if (queryParams.order.field !== selected) {
      handleOrderFieldChangeSetQueryParams({
        sizeOfPage: queryParams.order.sizeOfPage,
        field: selected,
        orderDirection: 0,
        currentPage: 1,
      });
    } else {
      if (queryParams.order.orderDirection === 0) {
        handleOrderFieldChangeSetQueryParams({
          sizeOfPage: queryParams.order.sizeOfPage,
          field: queryParams.order.field,
          orderDirection: 1,
          currentPage: queryParams.order.currentPage,
        });
      } else {
        handleOrderFieldChangeSetQueryParams({
          sizeOfPage: queryParams.order.sizeOfPage,
          field: 0,
          orderDirection: 0,
          currentPage: 1,
        });
      }
    }
  };

  const handleResponse = (response) => {
    setFormData(response.result.valueList);
    let totalPage =
      queryParams.order.sizeOfPage !== 0
        ? response.result.totalCount / queryParams.order.sizeOfPage
        : 0;

    if (totalPage !== totalItemsPerPage) {
      setTotalItemsPerPage(totalPage);
    }
  };

  const handleNewFilter = async () => {
    setQueryParams({
      filter: {
        search: filter.search,
      },
      order: {
        sizeOfPage: queryParams.order.sizeOfPage,
        field: queryParams.order.field,
        orderDirection: queryParams.order.orderDirection,
        currentPage: 1,
      },
    });
  };

  const handleSubmit = async () => {
    await GeneralFunction.evenResponse(
      '/api/v1/Unit/allWithDeleted',
      {
        search: filter.search,
        orderSetting: {
          startPosition:
            (queryParams.order.currentPage - 1) * queryParams.order.sizeOfPage,
          pageSize: queryParams.order.sizeOfPage,
          isOrder: true,
          orderField: queryParams.order.field,
          orderDirection: queryParams.order.orderDirection,
        },
      },
      'post',
      handleResponse
    );
  };

  const renderSortIcon = (columnName) => {
    if (queryParams.order.field === columnName) {
      return queryParams.order.orderDirection === 1 ? (
        <FontAwesomeIcon icon={faSortUp} />
      ) : (
        <FontAwesomeIcon icon={faSortDown} />
      );
    } else {
      return <FontAwesomeIcon icon={faSort} />;
    }
  };

  const handleOpenDelDialog = (id, title, name) => {
    setShowDelDialog(true);
    setFormDelete({ id: id, title: title, name: name });
  };

  const handleCloseDelDialog = () => {
    setShowDelDialog(false);
  };

  const handleDelete = async () => {
    await GeneralFunction.evenResponse(
      '/api/v1/Unit/delete/' + formDelete.id,
      {},
      'delete',
      async () => {
        await handleSubmit();
        handleCloseDelDialog();
      }
    );
  };

  const handleEdit = (id) => {
    navigate(`edit/${id}`);
  };

  return (
    <div className="container mt-4">
      <h1>Единицы измерения</h1>
      <div className="filter mb-3">
        <h5>Фильтр</h5>
        <label htmlFor="search" className="form-label">
          Наименование:
        </label>
        <input
          type="text"
          id="search"
          className="form-control formInputCon"
          name="search"
          value={filter.search}
          onChange={handleChange}
        />
        <button
          onClick={() => {
            handleNewFilter();
          }}
        >
          Поиск
        </button>
      </div>
      <div className="btnTool mb-3">
        <img
          className="createImg"
          src={createPNG}
          alt="Добавить"
          onClick={() => {
            handleAddClick();
          }}
        />
      </div>
      <div className="data mb-3">
        <table>
          <thead>
            <tr>
              <th>№</th>
              <th>Дата создания</th>
              <th>Дата редактирования</th>
              <th onClick={() => handleOrderFieldChange({ selected: 1 })}>
                {renderSortIcon(1)} Наименование
              </th>
              <th>...</th>
            </tr>
          </thead>
          <tbody>
            {formData &&
              formData.map((row, index) => (
                <tr key={row.id}>
                  <td>{++index}</td>
                  <td>
                    {moment
                      .utc(row.createDate)
                      .local()
                      .format('DD.MM.yyyy HH:mm')}
                  </td>
                  <td>
                    {moment(moment.utc(row.modifyDate).local()).format(
                      'DD.MM.yyyy HH:mm'
                    )}
                  </td>
                  <td>{row.name}</td>
                  <td>
                    {row.isDeleted === 1 ? (
                      <img
                        src={reDeletePNG}
                        className="deleteInTablle"
                        alt="Восстановить"
                        title="Восстановить"
                        onClick={() =>
                          handleOpenDelDialog(row.id, 'Восстановить', row.name)
                        }
                      />
                    ) : (
                      <>
                        <img
                          src={editPNG}
                          className="deleteInTablle"
                          alt="Редактировать"
                          title="Редактировать"
                          onClick={() => handleEdit(row.id)}
                        />

                        <img
                          src={deletePNG}
                          className="deleteInTablle"
                          alt="Удалить"
                          title="Удалить"
                          onClick={() =>
                            handleOpenDelDialog(row.id, 'Удалить', row.name)
                          }
                        />
                      </>
                    )}
                    <ConfirmationDialog
                      show={showDelDialog}
                      onClose={handleCloseDelDialog}
                      onConfirm={handleDelete}
                      formData={formDelete}
                    />
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {/* Pagination */}
        <ReactPaginate
          containerClassName={'pagination'}
          pageClassName={'page-item'}
          activeClassName={'active'}
          onPageChange={handlePageChange}
          pageCount={totalItemsPerPage}
          breakLabel="..."
          previousLabel="<"
          nextLabel=">"
        />
      </div>
    </div>
  );
};

export default UnitList;
