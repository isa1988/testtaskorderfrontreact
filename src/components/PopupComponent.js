import React, { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PopupComponent = ({ message, messageType }) => {
  useEffect(() => {
    if (messageType === 'success') {
      showSucces();
    } else if (messageType === 'error') {
      showError();
    } else if (messageType === 'info') {
      showInfo();
    }
  });
  const showSucces = () => {
    toast.success(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  const showError = () => {
    toast.error(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  const showInfo = () => {
    toast.info(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  return (
    <div>
      <ToastContainer autoClose={10000} />
    </div>
  );
};

export default PopupComponent;
