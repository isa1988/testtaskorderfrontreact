import queryString from 'query-string';

class QueryParamsHelper {
  static parse(locationSearch) {
    return queryString.parse(locationSearch, {
      parseNumbers: true,
      parseBooleans: true,
    });
  }

  static stringify(params) {
    return queryString.stringify(params);
  }
}

export default QueryParamsHelper;
